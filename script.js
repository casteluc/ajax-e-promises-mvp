var url = "https://treinamentoajax.herokuapp.com/messages"
var messages = []
const messagesDiv = document.getElementById("messages")
const sendButton = document.getElementById("send-button")
const getButton = document.getElementById("get-button")
const getByIdButton = document.getElementById("get-by-id-button")

messagesDiv.addEventListener("click", e => {
    if (e.target.className == "send-button") {
        putMessage(e.target)     
    }

    e.preventDefault()
})

messagesDiv.addEventListener("click", e => {
    if (e.target.className == "edit-button") {
        e.target.parentNode.parentNode.nextSibling.nextSibling.style.display = "block"
    }

    e.preventDefault()
})

messagesDiv.addEventListener("click", e => {
    if (e.target.className == "cancel-button") {
        e.target.parentNode.parentNode.parentNode.style.display = "none"
    }
})

messagesDiv.addEventListener("click", e => {
    if (e.target.className == "delete-button") {
        eraseMessage(e.target.name)
        e.preventDefault()  
    }
    
    e.preventDefault()
})

getByIdButton.addEventListener("click", e => {
    filterMessages()
    e.preventDefault()
})

sendButton.addEventListener("click", e => {
    postMessage()
    document.getElementById("name").value = ''
    document.getElementById("message").value = ''
    getAllMessages()
    e.preventDefault()
})

getButton.addEventListener("click", e => {
    getAllMessages()
    e.preventDefault()
})


async function getAllMessages() {
    eraseMessages()

    await fetch(url)
    .then(response => response.json())
    .then(response => {
        for (key in response) {
            messages.push(response[key])
        }
    })

    printCards()
}

function printCards() {
    messages.forEach(m => {
        let element = document.createElement("div")
        let h2 = document.createElement("h2")
        let divButtons = document.createElement("div")
        let editButton = document.createElement("input")
        let deleteButton = document.createElement("input")
        let p = document.createElement("p")
        let divTitle = document.createElement("div")
        let divEdit = document.createElement("div")
        
        h2.innerHTML = m["name"]
        p.innerHTML = m["message"]
        deleteButton.setAttribute("value", "Deletar")
        deleteButton.setAttribute("class", "delete-button")
        deleteButton.setAttribute("name", m["id"])
        deleteButton.setAttribute("type", "submit")
        editButton.setAttribute("value", "Editar mensagem")
        editButton.setAttribute("class", "edit-button")
        editButton.setAttribute("type", "submit")
        divButtons.setAttribute("id", "div-buttons")

        divEdit.setAttribute("class", "div-edit")
        divEdit.setAttribute("style", "display: none")
        divEdit.innerHTML = `
            <form>
                <label for="">Nome:</label>
                <input type="text" class="name" value="${m["name"]}">

                <label for="">Mensagem:</label>
                <input type="text" class="message" value="${m["message"]}">

                <div>
                    <input type="submit" value="Enviar" class="send-button" name="${m["id"]}">
                    <input type="submit" value="Cancelar" class="cancel-button"/> 
                </div>
            </form>
        `
        
        divButtons.appendChild(deleteButton)
        divButtons.appendChild(editButton)
        
        divTitle.appendChild(h2)
        divTitle.appendChild(divButtons)
        
        element.appendChild(divTitle)
        element.appendChild(p)
        element.appendChild(divEdit)
    
        document.getElementById("messages").appendChild(element)
    })
}

function eraseMessages() {
    messages = []
    messagesDiv.innerHTML = ''
}

function eraseMessage(id) {
    fetch(url + "/" + id, {method:"DELETE"}).then(response => {getAllMessages()})
}

function postMessage() {
    let fetchBody = {
        "message": {
            "name": document.getElementById("name").value,
            "message": document.getElementById("message").value
        }
    }

    let fetchConfig = {
        method: "POST",
        headers: {"Content-Type":"application/JSON"},
        body: JSON.stringify(fetchBody)
    }

    fetch(url, fetchConfig)
}

function putMessage(target) {
    let fetchBody = {
        "message": {
            "name": target.parentNode.previousElementSibling.previousElementSibling.previousElementSibling.value,
            "message": target.parentNode.previousElementSibling.value
        }
    }

    let fetchConfig = {
        method:"PUT",
        headers:{"Content-Type":"application/JSON"},
        body: JSON.stringify(fetchBody)
    }

    fetch(url + "/" + target.name, fetchConfig).then(response => getAllMessages())
}

async function filterMessages() {
    fetch(url)
    .then(response => response.json())
    .then(response => {
        for (key in response) {
            if (response[key]["id"] == document.getElementById("input-id").value) {
                eraseMessages()
                messages.push(response[key])
                printCards()
            }
        }
    })
    
}

getAllMessages()